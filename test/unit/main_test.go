package unit

import (
	"github.com/onsi/gomega"
	"testing"
)

func TestInitialTest(t *testing.T) {
	gomega.RegisterTestingT(t)
	gomega.Expect("iniciado").Should(gomega.BeEquivalentTo("iniciado"))
}
